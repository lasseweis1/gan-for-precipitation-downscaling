# GAN for precipitation downscaling

This project only contains the code which I used to downscale precipitation data using a Generative Adversarial Network (GAN).
This is supplementary material to my report on the Course Regional Climate Modelling with the Model CCLM (23S 411.086) at University of Graz.
